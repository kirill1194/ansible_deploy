FROM docker

# from https://github.com/psharkey/docker/blob/master/ansible/Dockerfile
RUN	apk --update add \
		bash \
		ca-certificates \
		git \
		less \
		openssl \
		openssh-client \
		p7zip \
		python \
		py-lxml \
		py-pip \
		rsync \
		sshpass \
		sudo \
		zip \
    	&& apk --update add --virtual \
		build-dependencies \
		python-dev \
		libffi-dev \
		openssl-dev \
		build-base \
	&& pip install --upgrade \
		pip \
		cffi \
	&& pip install \
		ansible  \
		ansible-lint \
	&& apk del build-dependencies \
	&& rm -rf /var/cache/apk/*

ADD requirements.yml requirements.yml
# RUN ansible-galaxy install -r requirements.yml

COPY roles/ /etc/ansible/roles/
